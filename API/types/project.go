package types

import (
	"github.com/graphql-go/graphql"
)

type Project struct {
	Id      int    `db:"id" json:"id"`
	Name    string `db:"name" json:"name"`
	Path    string `db:"path" json:"path"`
	Port    int    `db:"port" json:"port"`
	Running bool   `db:"running" json:"running"`
}

// Schema GraphQL
var ProjectType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Project",
	Fields: graphql.Fields{
		"id":      &graphql.Field{Type: graphql.Int},
		"name":    &graphql.Field{Type: graphql.String},
		"path":    &graphql.Field{Type: graphql.String},
		"port":    &graphql.Field{Type: graphql.Int},
		"running": &graphql.Field{Type: graphql.Boolean},
	},
})
