package mutations

import (
	"../tables"
	"github.com/graphql-go/graphql"
	"warwaone.com/Framework/JsonDatabase"
)

const TABLE_PROJECT = "project"

func GetRootFields(db JsonDatabase.Database) graphql.Fields {
	table := tables.ProjectTable{db.GetTable(TABLE_PROJECT)}
	return graphql.Fields{
		"createProject": GetCreateProjectMutation(table),
		"updateProject": GetUpdateProjectMutation(table),
		"toggleProject": GetToggleProjectMutation(table),
	}
}
