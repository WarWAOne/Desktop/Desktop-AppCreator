package mutations

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"

	"../tables"
	"../types"
	"github.com/graphql-go/graphql"
)

// GetCreateProjectMutation create new project and return it
func GetCreateProjectMutation(table tables.ProjectTable) *graphql.Field {
	return &graphql.Field{
		Type: types.ProjectType,
		Args: graphql.FieldConfigArgument{
			"name": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"path": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"port": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.Int),
			},
			"running": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.Boolean),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			item := &types.Project{
				Name:    params.Args["name"].(string),
				Path:    params.Args["path"].(string),
				Port:    params.Args["port"].(int),
				Running: params.Args["running"].(bool),
			}

			// TODO: generate id
			item.Id = 7

			table.Create(*item)
			createDir(item.Path + "/" + item.Name)
			createReactApp(item.Path + "/" + item.Name + "/" + strings.ToLower(item.Name))
			fmt.Println("Project " + item.Name + " has been created succesfuly in " + item.Path + " directory !")

			return item, nil
		},
	}
}

func GetUpdateProjectMutation(table tables.ProjectTable) *graphql.Field {
	return &graphql.Field{
		Type: types.ProjectType,
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.Int),
			},
			"name": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"path": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"port": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.Int),
			},
			"running": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.Boolean),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			item := &types.Project{
				Id:      params.Args["id"].(int),
				Name:    params.Args["name"].(string),
				Path:    params.Args["path"].(string),
				Port:    params.Args["port"].(int),
				Running: params.Args["running"].(bool),
			}

			table.Update(*item)

			return item, nil
		},
	}
}

func GetToggleProjectMutation(table tables.ProjectTable) *graphql.Field {
	return &graphql.Field{
		Type: types.ProjectType,
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.Int),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			item := &types.Project{
				Id: params.Args["id"].(int),
			}

			*item = table.Get(item.Id)
			appPath := item.Path + "/" + item.Name + "/" + strings.ToLower(item.Name)

			if item.Running {
				stopReactApp(appPath, item.Port)
				item.Running = false
			} else {
				startReactApp(appPath, item.Port)
				item.Running = true
			}

			table.Update(*item)

			return item, nil
		},
	}
}

// Create a directory in path
func createDir(path string) {
	errMkDir := os.Mkdir(path, 077)
	if errMkDir != nil {
		fmt.Println("Failed while creating project directory")
	}
}

// Create new react app with create-react-app
func createReactApp(path string) {
	wg := new(sync.WaitGroup)
	cmd := []string{"sudo npx create-react-app " + path}

	for _, str := range cmd {
		wg.Add(1)
		print(str)
		execCmd(str, wg)
	}
	wg.Wait()
}

// Start a react app on port
func startReactApp(appPath string, port int) {
	runCmd("export PORT=" + strconv.Itoa(port) + " && npm start --prex " + appPath)
}

// Stop a react app
func stopReactApp(appPath string, port int) {
	//runCmd("npm start --prex " + appPath + " -p " + string(port))
}

// Run one command
func runCmd(cmd string) {
	wg := new(sync.WaitGroup)
	cmds := []string{cmd}

	for _, str := range cmds {
		wg.Add(1)
		execCmd(str, wg)
	}
	wg.Wait()
}

// Execute several shell Commands and wait for
func execCmd(cmd string, wg *sync.WaitGroup) {
	fmt.Println("command is ", cmd)

	parts := strings.Fields(cmd)
	head := parts[0]
	parts = parts[1:len(parts)]

	out, err := exec.Command(head, parts...).Output()
	if err != nil {
		fmt.Printf("%s", err)
	}
	fmt.Printf("%s", out)
	wg.Done()
}
