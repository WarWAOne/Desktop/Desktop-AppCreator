package tables

import (
	"encoding/json"

	"../types"

	"warwaone.com/Framework/JsonDatabase"
)

type ProjectTable struct {
	JsonDatabase.Table
}

func (table *ProjectTable) Get(id int) types.Project {
	list := table.GetAll()
	for _, item := range list {
		itemJson, _ := json.Marshal(item)
		var item types.Project
		_ = json.Unmarshal(itemJson, &item)
		if item.Id == id {
			return item
		}
	}
	return types.Project{}
}

func (table *ProjectTable) Remove(id int) {
	list := table.GetAll()
	for index, item := range list {
		itemJson, _ := json.Marshal(item)
		var item types.Project
		_ = json.Unmarshal(itemJson, &item)
		if item.Id == id {
			list := removeItem(list, index)
			table.WriteJson(list)
		}
	}
}

func (table *ProjectTable) Update(item types.Project) {
	table.Remove(item.Id)
	table.Create(item)
}

func removeItem(slice []interface{}, s int) []interface{} {
	return append(slice[:s], slice[s+1:]...)
}

/*func (table *ProjectTable) Start(id int) {
	//app := chi.URLParam(req, "path")
	//app = strings.Replace(app, "-", "/", -1)

	// TODO: get port from database
	//port := "1003"

	// Run command to start project
	wg := new(sync.WaitGroup)
	cmd := []string{"npm start --prex " + app + " -p " + port}
	for _, str := range cmd {
		wg.Add(1)
		exe_cmd(str, wg)
	}
	wg.Wait()

	// TODO: Change status of app in table if no error
	// TODO: If error send json message error
}*/

// #TODO: Add id to projects
