package queries

import (
	"../tables"
	"../types"
	"github.com/graphql-go/graphql"
)

func GetProjectQuery(table tables.ProjectTable) *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.ProjectType),
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return table.GetAll(), nil
		},
	}
}
