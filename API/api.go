package main

import (
	"fmt"
	"net/http"

	"./mutations"
	"./queries"
	"warwaone.com/Framework/JsonDatabase"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
)

// TODO: Database on host machine ?
const DATABASE_FOLDER = "../database"
const TABLE_PROJECT = "project"
const API_NAME = "Application Creator API"
const PORT = ":3333"

func main() {

	db := JsonDatabase.Database{Path: DATABASE_FOLDER}
	db.CreateTable(JsonDatabase.Table{Name: TABLE_PROJECT})

	schemaConfig := graphql.SchemaConfig{
		Query: graphql.NewObject(graphql.ObjectConfig{
			Name:   "RootQuery",
			Fields: queries.GetRootFields(db),
		}),
		Mutation: graphql.NewObject(graphql.ObjectConfig{
			Name:   "RootMutation",
			Fields: mutations.GetRootFields(db),
		}),
	}

	schema, err := graphql.NewSchema(schemaConfig)

	if err != nil {
		fmt.Println("API >> Failed to create GraphQL schema, error:", err)
	}

	graphQLHttpHandler := handler.New(&handler.Config{
		Schema: &schema,
	})

	var httpHandler http.Handler = CORSMiddleware{graphQLHttpHandler}

	http.Handle("/", httpHandler)
	fmt.Println("API >> server listening on port ", PORT)
	http.ListenAndServe(PORT, nil)
}

type CORSMiddleware struct {
	http.Handler
}

func (cm CORSMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	cm.Handler.ServeHTTP(w, r)
}
