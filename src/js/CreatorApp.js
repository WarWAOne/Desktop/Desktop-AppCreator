import React from 'react';
import App from '../core/js/App.js';

import '../css/App.scss';
import settingsIcon from '../core/icons/settings.svg';
import closeIcon from '../core/icons/cross.svg';

import Dialog from '../core/js/components/Dialog.js';
import FormCreateApp from './components/FormCreateApp.js';
import Projects from './components/Projects.js';

import store from './stores/store.js';
import {Provider} from 'react-redux';

class CreatorApp extends App {
  constructor() {
    super();
    this.onClickMainAction = this.onClickMainAction.bind(this);
  }

  render() {
    return (
      <Provider store={store}>
        <App title="App Creator"
          settingsIcon={settingsIcon}
          mainActionText="Create App"
          mainAction={this.onClickMainAction}>
          {this.state.dialog}
          <Projects />
        </App>
      </Provider>// <List lists={this.state.projects}/> // <List lists={this.props.projects}/> 
    );// TODO: Give to lists= the list form store
  }

  onClickMainAction() {
    this.showDialog(
      <Dialog closeIcon={closeIcon} title="New Application">
        <FormCreateApp />
      </Dialog>
    );
  }
}

export default CreatorApp;