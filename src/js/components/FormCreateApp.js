import React, { Component } from 'react';

class FormCreateApp extends Component {

  constructor() {
    super();
    this.state = {
      name: '',
      path: '/home/',
      port: 3000
    }; 
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  render() {
    console.log(this.state.fields);
    return (
      <form onSubmit={this.onSubmit}>
        <input name="name" onChange={this.onChange} value={this.state.name} type="text" placeholder="Name..." />
        <input name="path" onChange={this.onChange} value={this.state.path} type="text" placeholder="Path..." />
        <input name="port" onChange={this.onChange} value={this.state.port} type="number"  max="9000" min="1"  />
        <input type="submit" className="button card" value="Create"/>
      </form>
    );
  }

  onSubmit(e) {
    e.preventDefault();
    fetch("http://localhost:3333/", {
      method: 'POST',
      body: JSON.stringify({
        "query": "mutation { createProject(name: \""+this.state.name+"\", path: \""+this.state.path+"\", port: "+this.state.port+", running: false){id, name, path, port, running}}"
      })
    })
    .then(res => res.json())
    .then(data => console.log(data))
    // TODO: add loader
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
}

export default FormCreateApp;
