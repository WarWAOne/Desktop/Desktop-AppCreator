import React, { Component } from 'react';
import menuIcon from '../../core/icons/menu.svg';
import PopupMenu from '../../core/js/components/PopupMenu.js';
import { connect } from 'react-redux';

class Project extends Component {

  constructor() {
    super()
    this.startStopApp = this.startStopApp.bind(this);
  }

  render() {
    var text = '';
    switch(this.props.status) {
      case "running":
        text = "Stop";
      break;
      case "starting":
        text = "starting...";
        break
      case "stoped":
        text = "starting...";
        break
      default:
      text = "error";
    }
    
    return (
      <div className="project card">
        <img alt="Menu" src={menuIcon} />
        <PopupMenu />
        <h1 className="project-title">{this.props.name}</h1>
        <h4 className="project-path">{this.props.path}</h4>
        <button className="project-toggle" onClick={this.startStopApp} type="button">
          {text}
        </button>
      </div>
    );
  }

  startStopApp() {
    // TODO: See video to see how to create action
  }

}

export default Project;

