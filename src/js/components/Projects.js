import React, { Component } from 'react'
import List from '../../core/js/components/List';
import {connect} from 'react-redux';
import { fetchProjects } from '../actions/projectActions';
import Project from '../components/Project.js';

class Projects extends Component {

    componentWillMount() {
        this.props.fetchProjects();
      }

  render() {
    const lists = 
    [{
        binder: this.bindProject,
        items: this.props.projects
    }];

    return (
        <List lists={lists} />
    );
  }

  bindProject(item) {
    return(
      <Project key={item.id} name={item.name} path={item.path} />
    );
  }
}



const mapStateToProps = state => ({
    projects: state.projects.items
  });

export default connect(mapStateToProps, {fetchProjects})(Projects)
