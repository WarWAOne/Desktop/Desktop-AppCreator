import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers'

const initialState = {};
const middleware = [thunk];

const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));

export default store;

/*
class ProjectStore extends EventEmitter {
  constructor() {
    super();
    this.items = [];
  }

  Project(name, path) {
    this.name = name;
    this.path = path;
  }

  create(project) {
    this.items.push(project);
    this.emit('change');
  }

  getAll() {
    /*$.getJSON("http://localhost:3333/projects", function(projects) {
      this.items = projects;
      this.emit('change');
    }.bind(this))
    .fail(function() {
      console.log( "error" );
    });
    return this.items;*/
  /*}
}*/

//const store = new ProjectStore;

//export default store;
