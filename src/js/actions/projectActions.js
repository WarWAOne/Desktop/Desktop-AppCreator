import {
    FETCH_PROJECTS,
    GET_PROJECT,
    CREATE_PROJECT,
    REMOVE_PROJECT,
    TOGGLE_PROJECT
} from './types';

// Action fetch all projects
export const fetchProjects = () => dispatch => {
    fetch("http://localhost:3333/", {
            method: 'POST',
            body: JSON.stringify({
                "query": "query {project{id, name, path}}"
            })
        })
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_PROJECTS, // type of action 
            projects: data.data.project // projects is the state name sent to the component
        }));
}

export const toggleProject = () => dispatch => {
    const id = 5; // TODO: GET REAL id
    // TODO: pass state status to starting
    fetch("http://localhost:3333", {
        method: 'POST',
        body: JSON.stringify({
            "query": "mutation {toggleProject(id: "+id+"){}}"
        })
    })
    .then(res => res.json())
    .then(data => dispatch({
        type: TOGGLE_PROJECT,
        status: "running"
    }));
    // TODO: If error in json change status: "stoped"
} 

export const getProject = () => dispatch => {
    fetch("http://localhost:3333", {
        method: 'POST',
        body: JSON.stringify({
            "query": "query {project{id, name, path}}"
        })
    })
    .then(res => res.json())
    .then(data => dispatch({
        type: GET_PROJECT,
        project: data.data.project
    }));
}
