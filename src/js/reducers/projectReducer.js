import {
    FETCH_PROJECTS,
    GET_PROJECT,
    CREATE_PROJECT,
    REMOVE_PROJECT,
    TOGGLE_PROJECT
} from '../actions/types';

const initialState = {
    items: [],
    item: {}
}

export default function(state = initialState, action) {
     switch(action.type) {
         case FETCH_PROJECTS:
            return {
                ...state,
                items: action.projects
            }
        default:
            return state;
     } 
}